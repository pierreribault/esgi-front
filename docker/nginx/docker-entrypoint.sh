#!/bin/sh

# Waiting dependencies
/wait

# Start Nginx
nginx -g "daemon off;"
